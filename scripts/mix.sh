#!/bin/bash

NUMBER=$(zenity --entry --title="YEAR" --text="Enter planner's year:")
WORKDIR=../$NUMBER/
FILE="$WORKDIR/README.md"

sel_file() {
  files=$(zenity --title "Select one or more files:"  --file-selection --multiple --filename="$WORKDIR/")
  [[ "$files" ]] || exit 1
  echo $files | tr "|" "\n" | while read file
  do
    echo "$file" >> /tmp/files.txt
  done
}

scale() {
  sel_file

  scale_factor=$(zenity --entry --title "Enter the scale factor" --text "Enter the scale factor:")
  [ -z "$scale_factor" ] && exit 1
  SCALE_FOLDER="$WORKDIR/thumbs"

  mkdir -p "$SCALE_FOLDER"

  IFS=$'\n'
  for FILE in $(cat /tmp/files.txt); do
    FILENAME=$(basename "$FILE")
    FILE_EXTENSION="${FILENAME##*.}" # Extract file extension
    OUTPUT_FILE="$SCALE_FOLDER/${FILENAME%.*}.$FILE_EXTENSION"
    ffmpeg -i "$FILE" -vf "scale=iw/$scale_factor:ih/$scale_factor" -c:a copy "$OUTPUT_FILE"
    echo "Scaled $FILENAME and saved to $SCALE_FOLDER"
  done

  rm /tmp/files.txt
  echo "Temporary files removed!"
  export SCALE_FOLDER
}

OUTPUT_HTML="$WORKDIR/README.md"
CSS_FILE="$WORKDIR/style.css"

write_gallery() {
    # Create CSS file if not exists
    [ ! -f "$CSS_FILE" ] && {
        cat <<CSS > "$CSS_FILE"
  .gallery { display: grid; grid-template-columns: repeat(auto-fill, minmax(200px, 1fr)); gap: 10px; }
  .thumbnail { width: 100%; height: auto; }
CSS
    }

    cat <<HTML > "$OUTPUT_HTML"
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
</head>
<body>

## AUTHOR

- [Francesco Zubani](https://fatualux.github.io/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)
- [Python](https://www.python.org/)
- [Flask](https://flask.palletsprojects.com/)

## ANNO $NUMBER

<div class="gallery">
HTML

    for THUMB in "$SCALE_FOLDER"/*; do
        [ -f "$THUMB" ] || continue

        FILENAME=$(basename "$THUMB")
        THUMBS_DIR=./thumbs
        IMAGE_NAME="${FILENAME%.*}"
        IMAGE_PATH=$(find "$WORKDIR" -type f -iname "$IMAGE_NAME.*" -print -quit)
        RELATIVE_IMAGE_PATH="${FILENAME}"
        RELATIVE_THUMB_PATH="${THUMBS_DIR}/${FILENAME}"

        cat <<HTML >> "$OUTPUT_HTML"
  <a href="$RELATIVE_IMAGE_PATH"><img class="thumbnail" src="$RELATIVE_THUMB_PATH" alt="$IMAGE_NAME"></a>
HTML
    done

    # Close HTML tags
    cat <<HTML >> "$OUTPUT_HTML"
</div>
</body>
</html>
HTML

    zenity --info --title="Script Finished" --text="Gallery $TITLE generated successfully."
}

scale
write_gallery
