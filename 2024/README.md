<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="style.css">
</head>
<body>

## AUTHOR

- [Francesco Zubani](https://fatualux.github.io/)

## AKNOWLEDGEMENTS

Only FOSS software was used to generate the gallery.

This work was made possible by the following projects:

- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)
- [Python](https://www.python.org/)
- [Flask](https://flask.palletsprojects.com/)

## ANNO 2024

<h1>Planner mensile</h1>
<div class="gallery">
  <a href="01-2024.png"><img class="thumbnail" src="./thumbs/01-2024.png" alt="01-2024"></a>
  <a href="02-2024.png"><img class="thumbnail" src="./thumbs/02-2024.png" alt="02-2024"></a>
  <a href="03-2024.png"><img class="thumbnail" src="./thumbs/03-2024.png" alt="03-2024"></a>
  <a href="04-2024.png"><img class="thumbnail" src="./thumbs/04-2024.png" alt="04-2024"></a>
  <a href="05-2024.png"><img class="thumbnail" src="./thumbs/05-2024.png" alt="05-2024"></a>
  <a href="06-2024.png"><img class="thumbnail" src="./thumbs/06-2024.png" alt="06-2024"></a>
  <a href="07-2024.png"><img class="thumbnail" src="./thumbs/07-2024.png" alt="07-2024"></a>
  <a href="08-2024.png"><img class="thumbnail" src="./thumbs/08-2024.png" alt="08-2024"></a>
  <a href="09-2024.png"><img class="thumbnail" src="./thumbs/09-2024.png" alt="09-2024"></a>
</div>
  <h1>Planner annuale formato A3</h1>
<div class="gallery">
  <a href="year_A3.png"><img class="thumbnail" src="./thumbs/year_A3.png" alt="year_A3"></a>
</div>
  <h1>Planner annuale formato A4</h1>
<div class="gallery">
  <a href="year_A4.png"><img class="thumbnail" src="./thumbs/year_A4.png" alt="year_A4"></a>
</div>
</body>
</html>
