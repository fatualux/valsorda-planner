# MAV_ValSorda - PLANNER

[![YEAR 2024](https://img.shields.io/badge/YEAR-2024-red)](./2024/README.md)

## LICENSE

[![License](https://img.shields.io/badge/License-CC%20BY--NC--ND-green)](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.en)

This project is licensed under the CC BY-NC-ND 4.0 Deed license.
See LICENSE file for more details.
